﻿using System;
using System.ComponentModel.DataAnnotations.Schema;


namespace HotelTransilvania.Models
{
    public class DetalleReserva
    {

        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int DetalleReservaId { get; set; }
        public int FacturaId { get; set; }
        public Factura Factura { get; set; }
        public int ReservacionId { get; set; }
        public Reservacion Reservacion { get; set; }
        public Decimal PrecioDetalle { get; set; }
        
    }
}