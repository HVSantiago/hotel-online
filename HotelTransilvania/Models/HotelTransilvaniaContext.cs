﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace HotelTransilvania.Models
{
    public class HotelTransilvaniaContext : DbContext
    {
        // You can add custom code to this file. Changes will not be overwritten.
        // 
        // If you want Entity Framework to drop and regenerate your database
        // automatically whenever you change your model schema, please use data migrations.
        // For more information refer to the documentation:
        // http://msdn.microsoft.com/en-us/data/jj591621.aspx
    
        public HotelTransilvaniaContext() : base("name=HotelTransilvaniaContext")
        {
        }

        public System.Data.Entity.DbSet<HotelTransilvania.Models.Admin> Admins { get; set; }

        public System.Data.Entity.DbSet<HotelTransilvania.Models.Rol> Rols { get; set; }

        public System.Data.Entity.DbSet<HotelTransilvania.Models.Reservacion> Reservacions { get; set; }

        public System.Data.Entity.DbSet<HotelTransilvania.Models.Cliente> Clientes { get; set; }

        public System.Data.Entity.DbSet<HotelTransilvania.Models.Habitacion> Habitacions { get; set; }

        public System.Data.Entity.DbSet<HotelTransilvania.Models.Empleado> Empleadoes { get; set; }

        public System.Data.Entity.DbSet<HotelTransilvania.Models.Imagen> Imagens { get; set; }

        public System.Data.Entity.DbSet<HotelTransilvania.Models.Galeria> Galerias { get; set; }

        public System.Data.Entity.DbSet<HotelTransilvania.Models.EstadoHabitacion> EstadoHabitacions { get; set; }

        public System.Data.Entity.DbSet<HotelTransilvania.Models.Factura> Facturas { get; set; }

        public System.Data.Entity.DbSet<HotelTransilvania.Models.DetalleReserva> DetalleReservas { get; set; }

        public System.Data.Entity.DbSet<HotelTransilvania.Models.Card> Cards { get; set; }
    }
}
