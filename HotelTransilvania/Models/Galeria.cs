﻿using System.ComponentModel.DataAnnotations.Schema;


namespace HotelTransilvania.Models
{
    public class Galeria
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int GaleriaId { get; set; }
        public int HabitacionId { get; set; }
        public Habitacion Habitacion { get; set; }

    }
}