﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace HotelTransilvania.Models
{
    public class Habitacion
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int HabitacionId { get; set; }
        [Required]
        public int NumeroPiso { get; set; }
        [Required]
        public int NumeroCamas { get; set; }
        [Required]
        public int NumeroSanitarios { get; set; }
        [Required]
        public int MaximoPersonas { get; set; }
        [Required]
        public Decimal PrecioNoche { get; set; }
        public int EstadoHabitacionId { get; set; }
        public EstadoHabitacion EstadoHabitacion { get; set; }
    }
}