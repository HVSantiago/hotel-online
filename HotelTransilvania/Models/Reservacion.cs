﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace HotelTransilvania.Models
{
    public class Reservacion
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ReservacionId { get; set; }
        [Required]
        public DateTime FechaIngreso { get; set; }
        [Required]
        public DateTime FechaEgreso { get; set; }
        [Required]
        public int CantidadDias { get; set; }
        public string Observaciones { get; set; }
   
        public int HabitacionId { get; set; }
        public Habitacion Habitacion { get; set; }
        public int ClienteId { get; set; }
        public Cliente Cliente { get; set; }


    }
}