﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;


namespace HotelTransilvania.Models
{
    public class Imagen
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ImagenId { get; set; }
        [Required]
        public string ImagenURL { get; set; }
        public int GaleriaId { get; set; }
        public Galeria Galeria { get; set; }

    }
}