﻿namespace HotelTransilvania.Models
{
    using System;
    using System.Data.Entity;
    using System.Linq;

    public class HotelContext : DbContext
    {
       
        public HotelContext()
            : base("name=HotelContext")
        {
        }

        public DbSet<Rol> Roles { get; set; }
        public DbSet<Admin> Admins { get; set; }
        public DbSet<Empleado> Empleados { get; set; }
        public DbSet<Card> Cards { get; set; }
        public DbSet<EstadoHabitacion> EstadoHabitaciones { get; set; }
        public DbSet<Cliente> Clientes { get; set; }
        public DbSet<Habitacion> Habitaciones { get; set; }
        public DbSet<Galeria> Galerias { get; set; }
        public DbSet<Imagen> Imagenes { get; set; }
        public DbSet<Reservacion> Reservaciones { get; set; }
        public DbSet<Factura> Facturas { get; set; }
        public DbSet<DetalleReserva> DetalleReservas { get; set; }
        
        
        
        
        
        
        
        
    }

}