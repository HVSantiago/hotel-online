﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;


namespace HotelTransilvania.Models
{
    public class Factura
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int FacturaId { get; set; }
        [Required]
        public DateTime FechaFacturacion { get; set; }
        public int ClienteId { get; set; }
        public Cliente Cliente { get; set; }
        
    }
}