﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;


namespace HotelTransilvania.Models
{
    public class EstadoHabitacion
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int EstadoHabitacionId { get; set; }
        [Required]
        public string NombreEstado { get; set; }

    }
}