﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;


namespace HotelTransilvania.Models
{
    public class Card
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int CardId { get; set; }
        [Required]
        public int NumeroDeTarjeta { get; set; }
        [Required]
        public DateTime DueDateCard { get; set; }
        [Required]
        public int CodeCard { get; set; }

    }
}