﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;


namespace HotelTransilvania.Models
{
    public class Cliente
    {
        
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ClienteId { get; set; }
        [Required]
        public string NombreCliente { get; set; }
        [Required]
        public string ApellidoCliente { get; set; }
        [Required]
        public string Correo { get; set; }
        [Required]
        public string Contrasena { get; set; }
        [Required]
        public string Direccion { get; set; }

        public int NumeroDeTarjeta { get; set; }
        public DateTime DueDateCard { get; set; }
        public int CodeCard { get; set; }

        public int RolId { get; set; }
        public Rol Rol { get; set; }
    }
}