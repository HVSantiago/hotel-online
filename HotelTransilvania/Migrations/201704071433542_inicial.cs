namespace HotelTransilvania.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class inicial : DbMigration
    {
        public override void Up()
        {
            
            
            CreateTable(
                "dbo.Rols",
                c => new
                    {
                        RolId = c.Int(nullable: false, identity: true),
                        NombreRol = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.RolId);
            
            CreateTable(
                "dbo.Cards",
                c => new
                    {
                        CardId = c.Int(nullable: false, identity: true),
                        NumeroDeTarjeta = c.Int(nullable: false),
                        DueDateCard = c.DateTime(nullable: false),
                        CodeCard = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.CardId);

            CreateTable(
                "dbo.Admins",
                c => new
                {
                    AdminId = c.Int(nullable: false, identity: true),
                    NombreAdmin = c.String(nullable: false),
                    ApellidoAdmin = c.String(nullable: false),
                    Correo = c.String(nullable: false),
                    Contrasena = c.String(nullable: false),
                    RolId = c.Int(nullable: false),
                })
                .PrimaryKey(t => t.AdminId)
                .ForeignKey("dbo.Rols", t => t.RolId, cascadeDelete: true)
                .Index(t => t.RolId);

            CreateTable(
                "dbo.Clientes",
                c => new
                    {
                        ClienteId = c.Int(nullable: false, identity: true),
                        NombreCliente = c.String(nullable: false),
                        ApellidoCliente = c.String(nullable: false),
                        Correo = c.String(nullable: false),
                        Contrasena = c.String(nullable: false),
                        Direccion = c.String(nullable: false),
                        NumeroDeTarjeta = c.Int(nullable: false),
                        DueDateCard = c.DateTime(nullable: false),
                        CodeCard = c.Int(nullable: false),
                        RolId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ClienteId)
                .ForeignKey("dbo.Rols", t => t.RolId, cascadeDelete: true)
                .Index(t => t.RolId);
            CreateTable(
               "dbo.Empleadoes",
               c => new
               {
                   EmpleadoId = c.Int(nullable: false, identity: true),
                   NombreEmpleado = c.String(nullable: false),
                   ApellidoEmpleado = c.String(nullable: false),
                   Correo = c.String(nullable: false),
                   Contrasena = c.String(nullable: false),
                   RolId = c.Int(nullable: false),
               })
               .PrimaryKey(t => t.EmpleadoId)
               .ForeignKey("dbo.Rols", t => t.RolId, cascadeDelete: true)
               .Index(t => t.RolId);

            CreateTable(
                "dbo.EstadoHabitacions",
                c => new
                {
                    EstadoHabitacionId = c.Int(nullable: false, identity: true),
                    NombreEstado = c.String(nullable: false),
                })
                .PrimaryKey(t => t.EstadoHabitacionId);

           
            
            CreateTable(
                "dbo.Facturas",
                c => new
                    {
                        FacturaId = c.Int(nullable: false, identity: true),
                        FechaFacturacion = c.DateTime(nullable: false),
                        ClienteId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.FacturaId)
                .ForeignKey("dbo.Clientes", t => t.ClienteId, cascadeDelete: true)
                .Index(t => t.ClienteId);
            
            
            
            CreateTable(
                "dbo.Habitacions",
                c => new
                    {
                        HabitacionId = c.Int(nullable: false, identity: true),
                        NumeroPiso = c.Int(nullable: false),
                        NumeroCamas = c.Int(nullable: false),
                        NumeroSanitarios = c.Int(nullable: false),
                        MaximoPersonas = c.Int(nullable: false),
                        PrecioNoche = c.Decimal(nullable: false, precision: 18, scale: 2),
                        EstadoHabitacionId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.HabitacionId)
                .ForeignKey("dbo.EstadoHabitacions", t => t.EstadoHabitacionId, cascadeDelete: true)
                .Index(t => t.EstadoHabitacionId);


            CreateTable(
                "dbo.Reservacions",
                c => new
                {
                    ReservacionId = c.Int(nullable: false, identity: true),
                    FechaIngreso = c.DateTime(nullable: false),
                    FechaEgreso = c.DateTime(nullable: false),
                    CantidadDias = c.Int(nullable: false),
                    Observaciones = c.String(),
                    HabitacionId = c.Int(nullable: false),
                    ClienteId = c.Int(nullable: false),
                })
                .PrimaryKey(t => t.ReservacionId)
                .ForeignKey("dbo.Clientes", t => t.ClienteId, cascadeDelete: true)
                .ForeignKey("dbo.Habitacions", t => t.HabitacionId, cascadeDelete: true)
                .Index(t => t.HabitacionId)
                .Index(t => t.ClienteId);

            CreateTable(
               "dbo.DetalleReservas",
               c => new
               {
                   DetalleReservaId = c.Int(nullable: false, identity: true),
                   FacturaId = c.Int(nullable: false),
                   ReservacionId = c.Int(nullable: false),
                   PrecioDetalle = c.Decimal(nullable: false, precision: 18, scale: 2),
               })
               .PrimaryKey(t => t.DetalleReservaId)
               .ForeignKey("dbo.Facturas", t => t.FacturaId, cascadeDelete: false)
               .ForeignKey("dbo.Reservacions", t => t.ReservacionId, cascadeDelete: false)
               .Index(t => t.FacturaId)
               .Index(t => t.ReservacionId);

            CreateTable(
                "dbo.Galerias",
                c => new
                    {
                        GaleriaId = c.Int(nullable: false, identity: true),
                        HabitacionId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.GaleriaId)
                .ForeignKey("dbo.Habitacions", t => t.HabitacionId, cascadeDelete: true)
                .Index(t => t.HabitacionId);
            
            CreateTable(
                "dbo.Imagens",
                c => new
                    {
                        ImagenId = c.Int(nullable: false, identity: true),
                        ImagenURL = c.String(nullable: false),
                        GaleriaId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ImagenId)
                .ForeignKey("dbo.Galerias", t => t.GaleriaId, cascadeDelete: true)
                .Index(t => t.GaleriaId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Imagens", "GaleriaId", "dbo.Galerias");
            DropForeignKey("dbo.Galerias", "HabitacionId", "dbo.Habitacions");
            DropForeignKey("dbo.Empleadoes", "RolId", "dbo.Rols");
            DropForeignKey("dbo.DetalleReservas", "ReservacionId", "dbo.Reservacions");
            DropForeignKey("dbo.Reservacions", "HabitacionId", "dbo.Habitacions");
            DropForeignKey("dbo.Habitacions", "EstadoHabitacionId", "dbo.EstadoHabitacions");
            DropForeignKey("dbo.Reservacions", "ClienteId", "dbo.Clientes");
            DropForeignKey("dbo.DetalleReservas", "FacturaId", "dbo.Facturas");
            DropForeignKey("dbo.Facturas", "ClienteId", "dbo.Clientes");
            DropForeignKey("dbo.Clientes", "RolId", "dbo.Rols");
            DropForeignKey("dbo.Admins", "RolId", "dbo.Rols");
            DropIndex("dbo.Imagens", new[] { "GaleriaId" });
            DropIndex("dbo.Galerias", new[] { "HabitacionId" });
            DropIndex("dbo.Empleadoes", new[] { "RolId" });
            DropIndex("dbo.Habitacions", new[] { "EstadoHabitacionId" });
            DropIndex("dbo.Reservacions", new[] { "ClienteId" });
            DropIndex("dbo.Reservacions", new[] { "HabitacionId" });
            DropIndex("dbo.Facturas", new[] { "ClienteId" });
            DropIndex("dbo.DetalleReservas", new[] { "ReservacionId" });
            DropIndex("dbo.DetalleReservas", new[] { "FacturaId" });
            DropIndex("dbo.Clientes", new[] { "RolId" });
            DropIndex("dbo.Admins", new[] { "RolId" });
            DropTable("dbo.Imagens");
            DropTable("dbo.Galerias");
            DropTable("dbo.Empleadoes");
            DropTable("dbo.EstadoHabitacions");
            DropTable("dbo.Habitacions");
            DropTable("dbo.Reservacions");
            DropTable("dbo.Facturas");
            DropTable("dbo.DetalleReservas");
            DropTable("dbo.Clientes");
            DropTable("dbo.Cards");
            DropTable("dbo.Rols");
            DropTable("dbo.Admins");
        }
    }
}
