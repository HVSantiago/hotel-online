﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using HotelTransilvania.Models;

namespace HotelTransilvania.Controllers
{
    public class EstadoHabitacionsController : ApiController
    {
        private HotelTransilvaniaContext db = new HotelTransilvaniaContext();

        // GET: api/EstadoHabitacions
        public IQueryable<EstadoHabitacion> GetEstadoHabitacions()
        {
            return db.EstadoHabitacions;
        }

        // GET: api/EstadoHabitacions/5
        [ResponseType(typeof(EstadoHabitacion))]
        public async Task<IHttpActionResult> GetEstadoHabitacion(int id)
        {
            EstadoHabitacion estadoHabitacion = await db.EstadoHabitacions.FindAsync(id);
            if (estadoHabitacion == null)
            {
                return NotFound();
            }

            return Ok(estadoHabitacion);
        }

        // PUT: api/EstadoHabitacions/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutEstadoHabitacion(int id, EstadoHabitacion estadoHabitacion)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != estadoHabitacion.EstadoHabitacionId)
            {
                return BadRequest();
            }

            db.Entry(estadoHabitacion).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!EstadoHabitacionExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/EstadoHabitacions
        [ResponseType(typeof(EstadoHabitacion))]
        public async Task<IHttpActionResult> PostEstadoHabitacion(EstadoHabitacion estadoHabitacion)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.EstadoHabitacions.Add(estadoHabitacion);
            await db.SaveChangesAsync();

            return CreatedAtRoute("DefaultApi", new { id = estadoHabitacion.EstadoHabitacionId }, estadoHabitacion);
        }

        // DELETE: api/EstadoHabitacions/5
        [ResponseType(typeof(EstadoHabitacion))]
        public async Task<IHttpActionResult> DeleteEstadoHabitacion(int id)
        {
            EstadoHabitacion estadoHabitacion = await db.EstadoHabitacions.FindAsync(id);
            if (estadoHabitacion == null)
            {
                return NotFound();
            }

            db.EstadoHabitacions.Remove(estadoHabitacion);
            await db.SaveChangesAsync();

            return Ok(estadoHabitacion);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool EstadoHabitacionExists(int id)
        {
            return db.EstadoHabitacions.Count(e => e.EstadoHabitacionId == id) > 0;
        }
    }
}