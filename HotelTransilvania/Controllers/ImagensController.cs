﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using HotelTransilvania.Models;

namespace HotelTransilvania.Controllers
{
    public class ImagensController : ApiController
    {
        private HotelTransilvaniaContext db = new HotelTransilvaniaContext();

        // GET: api/Imagens
        public IQueryable<Imagen> GetImagens()
        {
            return db.Imagens;
        }

        // GET: api/Imagens/5
        [ResponseType(typeof(Imagen))]
        public async Task<IHttpActionResult> GetImagen(int id)
        {
            Imagen imagen = await db.Imagens.FindAsync(id);
            if (imagen == null)
            {
                return NotFound();
            }

            return Ok(imagen);
        }

        // PUT: api/Imagens/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutImagen(int id, Imagen imagen)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != imagen.ImagenId)
            {
                return BadRequest();
            }

            db.Entry(imagen).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ImagenExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Imagens
        [ResponseType(typeof(Imagen))]
        public async Task<IHttpActionResult> PostImagen(Imagen imagen)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Imagens.Add(imagen);
            await db.SaveChangesAsync();

            return CreatedAtRoute("DefaultApi", new { id = imagen.ImagenId }, imagen);
        }

        // DELETE: api/Imagens/5
        [ResponseType(typeof(Imagen))]
        public async Task<IHttpActionResult> DeleteImagen(int id)
        {
            Imagen imagen = await db.Imagens.FindAsync(id);
            if (imagen == null)
            {
                return NotFound();
            }

            db.Imagens.Remove(imagen);
            await db.SaveChangesAsync();

            return Ok(imagen);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool ImagenExists(int id)
        {
            return db.Imagens.Count(e => e.ImagenId == id) > 0;
        }
    }
}