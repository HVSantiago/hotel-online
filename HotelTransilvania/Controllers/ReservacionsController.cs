﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using HotelTransilvania.Models;

namespace HotelTransilvania.Controllers
{
    public class ReservacionsController : ApiController
    {
        private HotelTransilvaniaContext db = new HotelTransilvaniaContext();

        // GET: api/Reservacions
        public IQueryable<Reservacion> GetReservacions()
        {
            return db.Reservacions;
        }

        // GET: api/Reservacions/5
        [ResponseType(typeof(Reservacion))]
        public async Task<IHttpActionResult> GetReservacion(int id)
        {
            Reservacion reservacion = await db.Reservacions.FindAsync(id);
            if (reservacion == null)
            {
                return NotFound();
            }

            return Ok(reservacion);
        }

        // PUT: api/Reservacions/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutReservacion(int id, Reservacion reservacion)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != reservacion.ReservacionId)
            {
                return BadRequest();
            }

            db.Entry(reservacion).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ReservacionExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Reservacions
        [ResponseType(typeof(Reservacion))]
        public async Task<IHttpActionResult> PostReservacion(Reservacion reservacion)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Reservacions.Add(reservacion);
            await db.SaveChangesAsync();

            return CreatedAtRoute("DefaultApi", new { id = reservacion.ReservacionId }, reservacion);
        }

        // DELETE: api/Reservacions/5
        [ResponseType(typeof(Reservacion))]
        public async Task<IHttpActionResult> DeleteReservacion(int id)
        {
            Reservacion reservacion = await db.Reservacions.FindAsync(id);
            if (reservacion == null)
            {
                return NotFound();
            }

            db.Reservacions.Remove(reservacion);
            await db.SaveChangesAsync();

            return Ok(reservacion);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool ReservacionExists(int id)
        {
            return db.Reservacions.Count(e => e.ReservacionId == id) > 0;
        }
    }
}