﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using HotelTransilvania.Models;

namespace HotelTransilvania.Controllers
{
    public class GaleriasController : ApiController
    {
        private HotelTransilvaniaContext db = new HotelTransilvaniaContext();

        // GET: api/Galerias
        public IQueryable<Galeria> GetGalerias()
        {
            return db.Galerias;
        }

        // GET: api/Galerias/5
        [ResponseType(typeof(Galeria))]
        public async Task<IHttpActionResult> GetGaleria(int id)
        {
            Galeria galeria = await db.Galerias.FindAsync(id);
            if (galeria == null)
            {
                return NotFound();
            }

            return Ok(galeria);
        }

        // PUT: api/Galerias/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutGaleria(int id, Galeria galeria)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != galeria.GaleriaId)
            {
                return BadRequest();
            }

            db.Entry(galeria).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!GaleriaExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Galerias
        [ResponseType(typeof(Galeria))]
        public async Task<IHttpActionResult> PostGaleria(Galeria galeria)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Galerias.Add(galeria);
            await db.SaveChangesAsync();

            return CreatedAtRoute("DefaultApi", new { id = galeria.GaleriaId }, galeria);
        }

        // DELETE: api/Galerias/5
        [ResponseType(typeof(Galeria))]
        public async Task<IHttpActionResult> DeleteGaleria(int id)
        {
            Galeria galeria = await db.Galerias.FindAsync(id);
            if (galeria == null)
            {
                return NotFound();
            }

            db.Galerias.Remove(galeria);
            await db.SaveChangesAsync();

            return Ok(galeria);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool GaleriaExists(int id)
        {
            return db.Galerias.Count(e => e.GaleriaId == id) > 0;
        }
    }
}