﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using HotelTransilvania.Models;

namespace HotelTransilvania.Controllers
{
    public class DetalleReservasController : ApiController
    {
        private HotelTransilvaniaContext db = new HotelTransilvaniaContext();

        // GET: api/DetalleReservas
        public IQueryable<DetalleReserva> GetDetalleReservas()
        {
            return db.DetalleReservas;
        }

        // GET: api/DetalleReservas/5
        [ResponseType(typeof(DetalleReserva))]
        public async Task<IHttpActionResult> GetDetalleReserva(int id)
        {
            DetalleReserva detalleReserva = await db.DetalleReservas.FindAsync(id);
            if (detalleReserva == null)
            {
                return NotFound();
            }

            return Ok(detalleReserva);
        }

        // PUT: api/DetalleReservas/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutDetalleReserva(int id, DetalleReserva detalleReserva)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != detalleReserva.DetalleReservaId)
            {
                return BadRequest();
            }

            db.Entry(detalleReserva).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!DetalleReservaExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/DetalleReservas
        [ResponseType(typeof(DetalleReserva))]
        public async Task<IHttpActionResult> PostDetalleReserva(DetalleReserva detalleReserva)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.DetalleReservas.Add(detalleReserva);
            await db.SaveChangesAsync();

            return CreatedAtRoute("DefaultApi", new { id = detalleReserva.DetalleReservaId }, detalleReserva);
        }

        // DELETE: api/DetalleReservas/5
        [ResponseType(typeof(DetalleReserva))]
        public async Task<IHttpActionResult> DeleteDetalleReserva(int id)
        {
            DetalleReserva detalleReserva = await db.DetalleReservas.FindAsync(id);
            if (detalleReserva == null)
            {
                return NotFound();
            }

            db.DetalleReservas.Remove(detalleReserva);
            await db.SaveChangesAsync();

            return Ok(detalleReserva);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool DetalleReservaExists(int id)
        {
            return db.DetalleReservas.Count(e => e.DetalleReservaId == id) > 0;
        }
    }
}