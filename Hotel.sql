CREATE DATABASE HotelTransilvania
USE HotelTransilvania


CREATE TABLE Rol(
idRol INT IDENTITY(1,1) NOT NULL,
nombreRol VARCHAR(15) NOT NULL,
PRIMARY KEY(idRol)
);


CREATE TABLE Admin(
idAdmin INT IDENTITY(1,1) NOT NULL,
nombreAdmin VARCHAR(40) NOT NULL,
apellidoAdmin VARCHAR(40) NOT NULL,
correo VARCHAR(40) NOT NULL,
contrasena VARCHAR(40) NOT NULL,
idRol INT NOT NULL,
PRIMARY KEY(idAdmin),
FOREIGN KEY (idRol) REFERENCES Rol(idRol)
);

CREATE TABLE Empleado(
idEmpleado INT IDENTITY(1,1) NOT NULL,
nombreAEmpleado VARCHAR(40) NOT NULL,
apellidoEmpleado VARCHAR(40) NOT NULL,
correo VARCHAR(40) NOT NULL,
contrasena VARCHAR(40) NOT NULL,
idRol INT NOT NULL,
PRIMARY KEY(idEmpleado),
FOREIGN KEY (idRol) REFERENCES Rol(idRol)
);


CREATE TABLE Card(
idCard INT IDENTITY(1,1) NOT NULL,
numeroDeTarjeta INT NULL,
dueDateCard DATE NULL,
codeCard INT NULL,
PRIMARY KEY(idCard)
);


CREATE TABLE EstadoHabitacion(
idEstado INT IDENTITY(1,1) NOT NULL,
nombreEstado VARCHAR(20) NOT NULL,
PRIMARY KEY(idEstado)
);

CREATE TABLE Cliente(
idCliente INT IDENTITY(1,1) NOT NULL,
nombreUsuario VARCHAR(40) NOT NULL,
apellidoUsuario VARCHAR(40) NOT NULL,
correo VARCHAR(40) NOT NULL,
contrasena VARCHAR(40) NOT NULL,
direccion VARCHAR(200) NOT NULL,
--Estos registros seran especilamente si el cliente
--desea ingresar su tardeja de credito o debito
numeroDeTarjeta INT NULL,
dueDateCard DATE NULL,
codeCard INT NULL,
---------------------------------------------------
idRol INT NOT NULL,
PRIMARY KEY(idCliente),
FOREIGN KEY (idRol) REFERENCES Rol(idRol)
);



CREATE TABLE Habitacion(
idHabitacion INT IDENTITY(1,1) NOT NULL,
numeroPiso INT NOT NULL,
numeroCamas INT NOT NULL,
numeroSanitarios INT NOT NULL,
maximoDePersonas INT NOT NULL,
precionPorNoche DECIMAL(5,5) NOT NULL,
idEstado INT NOT NULL,
PRIMARY KEY(idHabitacion),
FOREIGN KEY(idEstado) REFERENCES EstadoHabitacion(idEstado)
);

CREATE TABLE Galeria(
idGaleria INT IDENTITY(1,1) NOT NULL,
idHabitacion INT NOT NULL,
PRIMARY KEY(idGaleria),
FOREIGN KEY(idHabitacion) REFERENCES Habitacion(idHabitacion)
);

CREATE TABLE Imagen(
idImagen INT IDENTITY(1,1) NOT NULL,
imagen TEXT NULL,
idGaleria INT NOT NULL,
PRIMARY KEY(idImagen),
FOREIGN KEY(idGaleria) REFERENCES Galeria(idGaleria)
);


CREATE TABLE Reservacion(
idReservacion INT IDENTITY(1,1) NOT NULL,
fechaIngreso DATE NOT NULL,
fechaEgreso DATE NOT NULL,
cantidadDias INT NOT NULL,
observaciones TEXT NULL,
idHabitacion INT NOT NULL,
idCliente INT NOT NULL,
PRIMARY KEY(idReservacion),
FOREIGN KEY(idHabitacion) REFERENCES Habitacion(idHabitacion),
FOREIGN KEY(idCliente) REFERENCES Cliente(idCliente)
);


CREATE TABLE Factura(
idFactura INT IDENTITY(1,1) NOT NULL,
fechaFactura DATE NOT NULL,
idCliente INT NOT NULL,
PRIMARY KEY(idFactura),
FOREIGN KEY(idCliente) REFERENCES Cliente(idCliente)
)

CREATE TABLE DetalleReserva(
idDetalle INT IDENTITY(1,1) NOT NULL,
idFactura INT NOT NULL,
idReservacion INT NOT NULL,
precioDetalle DECIMAL(10,2) NOT NULL,
PRIMARY KEY(idDetalle),
FOREIGN KEY(idFactura) REFERENCES Factura(idFactura),
FOREIGN KEY(idReservacion) REFERENCES Reservacion(idReservacion)
);



